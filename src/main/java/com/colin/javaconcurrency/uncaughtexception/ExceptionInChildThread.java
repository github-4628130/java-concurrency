package com.colin.javaconcurrency.uncaughtexception;

/**
 * Created by Colin on 2020/3/14 14:03
 * email: colinzhaodong@gmail.com
 * desc: 单线程，抛出，处理，有异常堆栈 多线程，子线程发生异常，会有什么不同？
 *
 *
 * @author zhaod
 */
public class ExceptionInChildThread implements Runnable{
	public static void main(String[] args) {
		new Thread(new ExceptionInChildThread()).start();
		for (int i = 0; i < 1000; i++) {
			System.out.println(i);
		}
	}

	@Override
	public void run() {
		throw new RuntimeException();
	}
}
