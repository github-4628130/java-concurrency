package com.colin.javaconcurrency.createthreads;

/**
 * @author: BlueMelancholy
 * 2019/12/16 15:57
 * @desc Thread方式实现线程
 * @email zhaod@oceansoft.com.cn
 */
public class ThreadStyle extends Thread{
    @Override
    public void run() {
        System.out.println("Thread方式启动线程");
    }

    public static void main(String[] args) {
        new ThreadStyle().start();
    }
}
