package com.colin.javaconcurrency.threadobjectclasscommonmethods;

/**
 * Created by Colin on 2020/3/8 19:45
 * email: colinzhaodong@gmail.com
 * desc: //todo
 *
 * @author zhaod
 */
public class JoinPrinciple {
	public static void main(String[] args) throws InterruptedException {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(Thread.currentThread().getName() + "执行完毕");
			}
		});

		thread.start();
		System.out.println("开始等待子线程运行完毕");
		thread.join();
		System.out.println(thread.getState());
		//等价写法
        synchronized (thread) {
        	//主线程进入永久等待,子线程run方法执行完毕会调用notify方法
            thread.wait();
        }
		System.out.println("所有子线程执行完毕");
	}
}
