package com.colin.javaconcurrency.threadobjectclasscommonmethods;

/**
 * Created by Colin on 2020/3/8 18:55
 * email: colinzhaodong@gmail.com
 * desc: 展示线程sleep的时候不释放synchronized的monitor，等sleep时间到了以后，正常结束后才释放锁
 *
 * @author zhaod
 */
public class SleepDontReleaseMoniter implements Runnable{
	public static void main(String[] args) {
		SleepDontReleaseMoniter sleepDontReleaseMonitor = new SleepDontReleaseMoniter();
		new Thread(sleepDontReleaseMonitor).start();
		new Thread(sleepDontReleaseMonitor).start();
	}
	@Override
	public void run() {
		sync();
	}

	private synchronized void sync() {
		System.out.println("线程" + Thread.currentThread().getName() + "获取到了monitor。");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("线程" + Thread.currentThread().getName() + "退出了同步代码块");
	}
}
