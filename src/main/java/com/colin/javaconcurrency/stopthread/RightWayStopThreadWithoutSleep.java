package com.colin.javaconcurrency.stopthread;

/**
 * @author: BlueMelancholy
 * 2019/12/17 13:47
 * @desc   run方法没有sleep和wait方法时停止线程
 * @email zhaod@oceansoft.com.cn
 */
public class RightWayStopThreadWithoutSleep implements Runnable{

    @Override
    public void run() {
        int num = 0;
        while (!Thread.currentThread().isInterrupted() && num <= Integer.MAX_VALUE / 2){
            if (num % 10000 == 0){
                System.out.println(num + "是一万的倍数");
            }
            num++;
        }
        System.out.println("任务运行结束");
    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new RightWayStopThreadWithoutSleep());
        thread.start();
        Thread.sleep(1000);
        thread.interrupt();

    }
}
