package com.colin.javaconcurrency.stopthread.volatiledemo;

/**
 * Created by Colin on 2020/3/2 16:56
 * email: colinzhaodong@gmail.com
 * desc: 展示volatile的局限
 *
 * @author zhaod
 */
public class WrongWayVolatile implements Runnable{
	private volatile boolean canceled = false;

	@Override
	public void run() {
		int num = 0;
		try {
			while (num <= 100000 & !canceled){
				if (num % 100 == 0){
					System.out.println(num+ "是100的倍数");
				}
				num++;
				Thread.sleep(1);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws InterruptedException {
		WrongWayVolatile wrongWayVolatile = new WrongWayVolatile();
		Thread thread = new Thread(wrongWayVolatile);
		thread.start();
		Thread.sleep(1000);
		wrongWayVolatile.canceled = true;
	}
}
