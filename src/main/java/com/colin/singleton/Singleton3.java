package com.colin.singleton;

/**
 * Created by Colin on 2020/3/21 14:37
 * email: colinzhaodong@gmail.com
 * desc:3.懒汉式（不安全，不推荐）
 *
 * @author zhaod
 */
public class Singleton3 {
	private static Singleton3 instance;
	private Singleton3(){

	}
	public static Singleton3 getInstance(){
		if (instance == null){
			instance = new Singleton3();
		}
		return instance;
	}
}
