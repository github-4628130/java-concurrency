package com.colin.singleton;

/**
 * Created by Colin on 2020/3/21 14:37
 * email: colinzhaodong@gmail.com
 * desc: 4.懒汉式（线程安全，synchronized效率低，不推荐用）
 *
 * @author zhaod
 */
public class Singleton4 {
	private static Singleton4 instance;
	private Singleton4(){

	}
	public synchronized static Singleton4 getInstance(){
		if (instance == null){
			instance = new Singleton4();
		}
		return instance;
	}
}
